/*
    Video: https://www.youtube.com/watch?v=38aE1lSAJZ8
    Don't forget to disable less secure app from Gmail: https://myaccount.google.com/lesssecureapps TODO:
*/

require('dotenv').config();

let a=4;


function makeid(a) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 4; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }


const nodemailer = require('nodemailer');

// Step 1
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD 
    }
});

// Step 2
// Step 3
let mailOptions = {
    from: 'isjposte08@gmail.com', // TODO: email sender
    to: 'leonel65kuaya@gmail.com', // TODO: email receiver
    subject: 'Mysms verification code',
    text: 'votre code de verification est:  MYSMS-'+ makeid()+'<>',
    html: 'Votre code de verification est: MYSMS-'+makeid()+'<br><p><a href="https://www.google.com/">cliquez-ici pour activer votre compte</a></p>',

};

// Step 4
transporter.sendMail(mailOptions, (err, data) => {
    if (err) {
        return console.log('Error occurs');
    }
    return console.log('Email sent!!!');
});